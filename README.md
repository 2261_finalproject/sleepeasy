# README #



### What is this repository for? ###

This Repository will be used for the Final Application development course.


### How do I get set up? ###


Once the repository has been cloned there will be the sleepeasy folder. Copy this folder to your web root. Once that has been done open up Mysql and import the sleepeasy.sql file. Next step is to ensure that you place the correct credentials for the database connections. This will be done by editing the database.php file which is located in the the app/Config folder.
After that the project should be setup as long as the webserver and database server are running. The website should be located at  "yourhost/sleepeasy"  , so if it is on your localhost it will be localhost/sleepeasy. Finally there are some test accounts included in the project, they are manager,employee, and guests all will a password of 123. They all have the permissions that are appropriate to their usernames. Any questions feel free to contact the sleepeasy team.
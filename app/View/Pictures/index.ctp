  <div id="hotelCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#hotelCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#hotelCarousel" data-slide-to="1"></li>
        <li data-target="#hotelCarousel" data-slide-to="2"></li>
        
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <h4 class="text-center">Executive Room</h4>
          <?php echo $this->Html->image('3d.jpg',array('alt'=>'First Slide'));?>
          
          <div class="container">
            
            
          </div>
        </div>
        <div class="item">
            <h4 class="text-center">Outside of Hotel</h4>
                    <?php echo $this->Html->image('outside.jpg',array('alt'=>'Second Slide'));?>

          <div class="container">
            
          </div>
        </div>
        
        <div class="item">
        <h4 class="text-center">Executive Bathroom</h4>
        <?php echo $this->Html->image('Bathroom-Shower-View.jpg',array('alt'=>'Third Slide'));?>
          <div class="container">
            
          </div>
        </div>
      </div>
      
      <a class="left carousel-control" href="#hotelCarousel" role="button" data-slide="prev">
        <i class="fa fa-step-backward fa-5x"></i>

        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#hotelCarousel" role="button" data-slide="next">
        <i class="fa fa-step-forward fa-5x"></i>

        <span class="sr-only">Next</span>
      </a>
    </div>
</div>


<script type="text/javascript" src="https://www.google.com/jsapi"></script>

  <script type="text/javascript">

  
    
    

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
             $.post('/sleepeasy/charges/weekreport', function (result) {
                 accounts = JSON.parse(result);

           
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Guest');
        data.addColumn('number', 'Balance');
        
        
        if(typeof accounts[1]=== 'undefined' ){
            data.addRows([
                ['Room Booking', parseFloat(Math.round(accounts[0][0].total).toFixed(2)) ],
                ['Room Service',parseFloat(0.00)]]);
            
        }
        
       else if(typeof accounts[0]==='undefined'){
            
            data.addRows([
                ['Room Booking', parseFloat(0.00) ],
                ['Room Service',parseFloat(Math.round(accounts[1][0].total).toPrecision(2))]
            ]);
        }
        
        else{
            
            data.addRows([
                ['Room Booking', parseFloat(Math.round(accounts[0][0].total).toFixed(2)) ],
                ['Room Service', parseFloat(Math.round(accounts[1][0].total).toPrecision(2))]
            ]);
        }
        
            
        

        // Set chart options
        var options = {'title':'Monthly Room Charges by type',
                       'width':600,
                       'height':500,
                       'pieSliceText':'value'
                       
        
                        };
                       

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
        });
      }
        
    </script>
    <div >
        <div  id="chart_div"></div>
    </div>

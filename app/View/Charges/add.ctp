<?php

$chargeTypeList = array();

foreach($chargeType as $chrgType){
   $chargeTypeList =   array_merge ($chargeTypeList, array($chrgType['ChargeType']['title']=>$chrgType['ChargeType']['title']));
}

echo $this->Form->create('Charge');
echo $this->Form->input('Charge.charge_type',array(
    'type'=>'select','options'=>$chargeTypeList
));
echo $this->Form->input('Charge.user_id',array(
    'type'=>'select','options'=>$users
));
echo $this->Form->input('Charge.charge_amount',array(
    'min'=>0,'step'=>.01,'selected'=>0.00)
);

$options = array('label'=>'Add Charge','class'=>'btn btn-primary');


echo $this->Form->end($options);
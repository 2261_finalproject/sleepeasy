<?php
echo $this->Form->create('User');
echo $this->Form->input('id',array('type'=>'hidden'));
echo $this->Form->input('username',array('type'=>'text', 'class'=>'form-control','label'=>'User Name'));
echo $this->Form->input('password',array('type'=>'password', 'class'=>'form-control','label'=>'Password'));
echo $this->Form->input('password_confirm',array('type'=>'password', 'class'=>'form-control','label'=>'Confirm Password'));
echo $this->Form->input('address',array('rows'=>3,'class'=>'form-control','label'=>'Address'));
echo $this->Form->input('city',array('type'=>'text', 'class'=>'form-control','label'=>'City'));
echo $this->Form->input('postal',array('type'=>'text', 'class'=>'form-control','label'=>'Postal Code'));
echo $this->Form->input('home_phone',array('type'=>'text', 'class'=>'form-control','label'=>'Home Phone'));
echo $this->Form->input('alt_phone',array('type'=>'text', 'class'=>'form-control','label'=>'Alternative Phone'));
echo $this->Form->end('Update');
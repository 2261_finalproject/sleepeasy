<?php
/**
 * Admin user list view
 * User: Tim
 * Date: 2/10/2015
 * Time: 8:13 PM
 */
?>
<h1>Registered Users</h1>
<table>
    <tr>
        <th><?php echo $this->Paginator->sort('User.username','Username'); ?></th>
        <th>Full Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Address</th>
        <th><?php echo $this->Paginator->sort('User.user_role','User Type'); ?></th>
        <th></th>
        <th></th>
    </tr>
    
    <?php foreach($users as $user) { ?>
    <tr>
        <td><?php echo $user['User']['username']; ?></td>
        <td><?php echo $user['User']['first_name'] . " " . $user['User']['last_name']; ?></td>
        <td><?php echo $user['User']['email']; ?></td>
        <td><?php echo $user['User']['phone']; ?></td>
        <td><?php echo $user['User']['address'] . ", " . $user['User']['city'] . ", " . $user['User']['postal_code']; ?></td>
        <td><?php echo $user['User']['user_role']; ?></td>
        <td><?php echo $this->Html->link('Edit', 
                                        array('controller'=>'users', 'action'=>'edit', $user['User']['id']),
                                        array('class'=>'btn btn-primary')); ?></td>
        <td><?php echo $this->Form->postLink('Delete',
                                            array('controller'=>'users', 'action'=>'delete', $user['User']['id']), 
                                            array('confirm'=>'Delete this user. Are you sure?', 'class'=>'btn btn-warning')); ?></td>
    </tr>
    <?php } ?>
</table>



<?php

//old list

//foreach ($users as $user) {
//    echo "<div class='col-md-4'>";
//    
//    echo "User:  " . $user['User']['first_name'] . " " . $user['User']['last_name']." ";
//    echo "</div>";
//    echo "<div class='col-md-4'>";
//    echo $this->Html->link(
//    'Edit',
//    array('controller' => 'users', 'action' => 'edit', $user['User']['id']),
//            array('class'=>'btn btn-primary')
//    
//);
//  echo "</div>";
//  echo "<div class='col-md-4'>";
//    echo "  ";
//    echo $this->Form->postLink(
//        'Delete',
//        array('controller' => 'users', 'action' => 'delete',$user['User']['id'],  ),
//            
//        array('confirm' => 'Are you sure you wish to this user?',
//            'class'=>'btn btn-danger')
//
//    );
//    echo"</div>";
//   
//    echo "<br/>";
//}
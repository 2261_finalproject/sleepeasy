<?php

echo $this->Form->create('User');
?>

    <fieldset>
        <legend>Login Information</legend>

        <?php
        echo $this->Form->input('username', array('type' => 'text', 'class' => 'form-control', 'label' => 'User Name'));
        echo $this->Form->input('password_update', array('type' => 'password', 'required'=>false,'class' => 'form-control', 'label' => 'Password'));
        if($loggedIn['user_role'] ==  'Manager'){
            echo $this->Form->input('user_role',array('class'=>'form-control' ,'type'=>'select','options'=>array('Manager'=>'Manager','Employee'=>'Employee','Guest'=>'Guest')));
        }
        ?>
    </fieldset>
    <fieldset>
        <legend>Contact Information</legend>
        <?php
        echo $this->Form->input('first_name', array('type' => 'text', 'class' => 'form-control', 'label' => 'First Name'));
        echo $this->Form->input('last_name', array('type' => 'text', 'class' => 'form-control', 'label' => 'Last Name'));
        echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control', 'label' => 'email'));
        echo $this->Form->input('address', array('rows' => 3, 'class' => 'form-control', 'label' => 'Address'));
        echo $this->Form->input('city', array('type' => 'text', 'class' => 'form-control', 'label' => 'City'));
        echo $this->Form->input('postal_code', array('type' => 'text', 'class' => 'form-control', 'label' => 'Postal Code'));
        echo $this->Form->input('phone', array('type' => 'text', 'class' => 'form-control', 'label' => 'Home Phone'));
        ?>
    </fieldset>
    <fieldset>
        <legend>Payment Information</legend>
        <?php
        echo $this->Form->input('payment_type', array('options' => array('VISA'=>'VISA','MASTERCARD'=>'MASTERCARD','AMERICAN EXPRESS'=>'AMERICAN EXPRESS'), 'class' => 'form-control', 'label' => 'Payment Type'));
        echo $this->Form->input('cardholder_name', array('type' => 'text', 'class' => 'form-control', 'label' => 'Cardholder Name'));
        echo $this->Form->input('card_number', array('type' => 'password', 'class' => 'form-control', 'label' => 'Card Number'));
        echo $this->Form->input('User.card_expiration.day',array('type'=>'hidden','value'=>'1'));
        echo $this->Form->input('card_expiration', array(
            'type'=>'date',
            'label' => 'Card Expiration',
            'dateFormat' => 'M-Y',
            
        ));
        ?>
    </fieldset>


<?php
echo $this->Form->submit();
echo $this->Form->end();
?>
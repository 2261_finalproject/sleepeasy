<h1 class="alert alert-info">Create an Account</h1>
<?php
//creating the registration form
echo $this->Form->create('User', array('id' => 'registrationForm'));
?>

<fieldset>
    <!-- Creating the information section -->
    <legend>Login Information</legend>
    <?php
    echo $this->Form->input('username', array('type' => 'text', 'id' => 'username', 'class' => 'form-control', 'label' => 'User Name'));
    echo "<div id='username_feedback'></div>";
    echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control', 'label' => 'Password'));
    ?>
</fieldset>
<fieldset>
    <!-- Contact Information Section -->
    <legend>Contact Information</legend>
    <?php
    echo $this->Form->input('first_name', array('type' => 'text', 'class' => 'form-control', 'label' => 'First Name'));

    echo $this->Form->input('last_name', array('type' => 'text', 'class' => 'form-control', 'label' => 'Last Name'));
    echo $this->Form->input('email', array('type' => 'email', 'class' => 'form-control', 'label' => 'email'));
    echo $this->Form->input('address', array('rows' => 3, 'class' => 'form-control', 'label' => 'Address'));
    echo $this->Form->input('city', array('type' => 'text', 'class' => 'form-control', 'label' => 'City'));
    echo $this->Form->input('postal_code', array('type' => 'text', 'class' => 'form-control', 'label' => 'Postal Code'));
    echo $this->Form->input('phone', array('type' => 'text', 'class' => 'form-control', 'label' => 'Home Phone'));
    ?>
</fieldset>
<fieldset>

    <!-- Payment Information Section -->
    <legend>Payment Information</legend>
    <?php
    echo $this->Form->input('payment_type', array('options' => array('VISA' => 'VISA', 'MASTERCARD' => 'MASTERCARD', 'AMERICAN EXPRESS' => 'AMERICAN EXPRESS'), 'class' => 'form-control', 'label' => 'Payment Type'));
    echo $this->Form->input('cardholder_name', array('type' => 'text', 'class' => 'form-control', 'label' => 'Cardholder Name'));
    echo $this->Form->input('card_number', array('type' => 'text', 'class' => 'form-control', 'label' => 'Card Number', 'id' => 'card_number'));
    echo $this->Form->input('card_expiration.day',array('type'=>'hidden','value'=>'1'));
    echo $this->Form->input('card_expiration', array(
        'type' => 'date',
        'label' => 'Card Expiration',
        'dateFormat' => 'MY',
    ));
    ?>
</fieldset>
<!-- Creating the submission button -->
<?php
echo $this->Form->submit('Register', array('id' => 'submitBtn'));
echo $this->Form->end();
?>

<!-- This Script will be used to validate the user's username and disable the 
button if the username is take -->
<script>
    $(document).ready(function () {

        
        $('#username_feedback').hide();
        //hiding the warning when the user enters the username text box
        $('#username').focus(function () {

            $('#username_feedback').hide();

        });

        //when the user leaves the box
        $('#username').blur(function () {

            //Below post function is using check_username method of users controller.            

            $.post('/sleepeasy/users/check_username', {username: $('#username').val()}, function (result) {

                
                //checking if the username is available
                if (result === "Username available") {
                    //display the available message for 2 seconds and then hiding
                    $('#username_feedback').attr('class','alert alert-success');
                    
                    $('#username_feedback').html(result).show().delay(2000).fadeOut();
                    //enabling the submit button
                    $('input[type="submit"]').attr('disabled', false);
                }
                else {
                    //displaying the warning message
                    $('#username_feedback').attr('class','alert alert-danger');
                    
                    $('#username_feedback').html(result).show();
                    //disabling the button
                    $('input[type="submit"]').attr('disabled', true);
                }




            });

        });

    });
</script>

<h1 class="bg-success">Reports</h1>

    <br/>

    <div class="col-md-4  col-sm-12 col-xs-12">
        <a class="btn btn-primary" href="/sleepeasy/accounts/reportview">Account Balance Report</a>
    </div>
    
    <div class="col-md-4 col-sm-12 col-xs-12">
        <a class="btn btn-primary" href="/sleepeasy/charges/weekreportview">Weekly Charges by type</a>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
        <a class="btn btn-primary" href="/sleepeasy/reservation/roomtypereportview">Reservations by room type</a>
    </div>
<?php

echo $this->Form->create('Room');
?>

    <fieldset>
        <legend>Edit Room</legend>

        <?php
        echo $this->Form->input('room_number', array('type' => 'text', 'class' => 'form-control', 'label' => 'Room Number'));
        echo $this->Form->input('room_type', array('type' => 'text', 'class' => 'form-control', 'label' => 'Room Type'));
        ?>
    </fieldset>
<?php
echo $this->Form->submit();
echo $this->Form->end();
?>
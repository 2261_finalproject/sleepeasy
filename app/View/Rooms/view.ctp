

<h1>Rooms</h1>
<div class="text-right">
<a class="btn btn-success" href="/sleepeasy/rooms/add">Add Room</a>
</div>
<table>
    <tr>
        <th><?php echo $this->Paginator->sort('Room.room_number','Room Number'); ?></th>
        <th><?php echo $this->Paginator->sort('Room.room_type','Room Type'); ?></th>
        <th></th>
        <th></th>
        
    </tr>
    <!-- Here is where we loop through the rooms -->
    <?php foreach ($rooms as $room): ?>
    <tr>
        <td><?php echo $room['Room']['room_number']; ?></td>
        <td><?php echo $room['RoomType']['title']; ?></td>
        <td><?php echo $this->Html->link('Edit', 
                                        array('controller'=>'rooms', 'action'=>'edit', $room['Room']['room_number']),
                                        array('class'=>'btn btn-primary')); ?></td>
        <td><?php echo $this->Form->postLink('Delete',
                                            array('controller'=>'rooms', 'action'=>'delete', $room['Room']['room_number']), 
                                            array('confirm'=>'Delete this room. Are you sure?', 'class'=>'btn btn-warning')); ?></td>
    </tr>
    <?php endforeach; ?>
    
</table>



<h1 class="alert alert-info">Room Search</h1>
<?php



/**
 * Creating the Room Search Form, and using the custom validation rules 
 * in the reservation model
 */

echo $this->Form->create('RoomSearch',array('novalidate'=>true));
echo "<div>";
echo $this->Form->label('Select a RoomType');

echo $this->Form->input('Reservation.room_type',array('options'=>$roomType ,'type'=>'select', 'class'=>'form-control'));
echo "<span id='roomType_feedback' class='alert alert-danger' ></span>";
echo "</div>";
echo $this->Form->input('Reservation.number_of_guests',array('class'=>'form-control' , 'min'=>1, 'max'=>4 , 'default'=>'1','required'=>true));

echo "<div>";
echo $this->Form->input('Reservation.checkin'
  ,array(
    'selected'=>
    array(
     'year'=> date('Y'),
     'month'=>date('M'),
     'day'=>date('D')
     ),
    'label'=>'Check in Date ',
    'minYear'=>date('Y'),
    'minMonth'=>date('m')


    ));
echo "<span id='checkin_feedback' class='alert alert-danger' ></span>";
echo "</div>";

echo "<div>";
echo $this->Form->input('Reservation.checkout' , array(
  'selected'=>array(
   'day'=> date('j')+1,
   'month'=>date('M'),
   'year'=>date('Y')

   ),
  'minYear'=>date('Y'),
    
  'label'=>'CheckOut Date '
  )
);
echo "<span id='checkout_feedback' class='alert alert-danger' ></span>";
echo "</div>";


echo "<div class='center-text'>";

//button to submit
echo $this->Form->button('Search',array('type'=>'button' , 'id'=>'searchBtn','class' =>'btn btn-success'));
echo " ";
//button to cancel
echo $this->Html->link('Cancel',array('controller'=>'users','action'=>'index' ),array('class'=>'btn btn-danger'));
echo "</div>";
echo $this->Form->end();

//checking if the form was submitted
if(isset($query2)){
  ?>
  <table>
    <tr>
      <th>Available Rooms</th>
    </tr>
    <?php
    
    //looping through the available rooms
    for($i = 0; $i<count($query2);$i++){
      echo "<tr><td>".$query2[$i]['r']['room_number'];
      echo "<br/>";

      echo  $this->Html->image('room1.jpg', array('alt' => 'CakePHP'));

      echo "</td><td>";


        //posting the option to book the room
      echo      $this->Form->postLink(
        'Book Room',
        array('controller' => 'reservation', 'action' => 'booking',$query2[$i]['r']['room_number']),

        array('confirm' => 'Are you sure you wish to book this room', 'class'=>'btn btn-success'));




      echo    "</td>";
      echo "<td>";
      echo $this->Html->link(
        'Return Home',
        array('controller' => 'users', 'action' => 'index'),
        array('class'=>'btn btn-primary')

        );
      echo "<td/>".
      "</tr>";
    }
    if(count($query2)==0){
     echo "<tr><td><h1 class='alert alert-danger alert-dismissible'>Thats Embarassing No Rooms Available</h1></td></tr>";
   }
   echo"<br/>";?>
 </table>
 <?php  
 
 echo "<br/>";
 
// print_r($data);
}
?>
<script>



  $(document).ready(function () {
 //var checkin;


    $('#roomType_feedback').hide();
    $('#checkin_feedback').hide();
    $('#checkout_feedback').hide();
        /**
         * 
         * @returns {Number}
         * Checking if there is a valid selection for the room type
         */
         function checkRoom(){

            //grabbing th reservation type
            var roomType = $('#ReservationRoomType').val();
            if (roomType === "" || roomType === null) {
                    //If there is no selection then setting the warning text
                    //displaying it.
                    $('#roomType_feedback').text(" Must Include the room type");
                    $('#roomType_feedback').show();

                     //returning 0 to indicate not a valid selection
                     return 0;
                   }
                   else {

                    //hiding the message
                    $('#roomType_feedback').show().delay(0).fadeOut();
                    
                    //return 0 to indicate the room type is valid
                    return 1;
                  }

                }


               function checkinDate(){
                  var checkinDay = $('#ReservationCheckinDay').val();
                  var checkinMonth = $('#ReservationCheckinMonth').val();
                  var checkinYear = $('#ReservationCheckinYear').val();

                  var dateHolder = new Date();

                  var today = new Date();

                  var checkin = new Date(checkinYear+"-"+checkinMonth+"-"+checkinDay);
                  var checkinCorrected = new Date(checkin);
                  checkinCorrected.setDate(checkin.getDate()+1);

                  //alert(checkinCorrected);
                return  new Date(checkinCorrected);




                }


                 function checkoutDate(){
                  var checkoutDay = $('#ReservationCheckoutDay').val();
                  var checkoutMonth = $('#ReservationCheckoutMonth').val();
                  var checkoutYear = $('#ReservationCheckoutYear').val();
                  var  checkout = new Date(checkoutYear+"-"+checkoutMonth+"-"+checkoutDay);
                  var checkoutCorrected = new Date(checkout);
                  checkoutCorrected.setDate(checkout.getDate()+1);

                  //alert(checkoutCorrected);

                 return new Date(checkoutCorrected);

                }


                function validateDate(){
                 var errors = false;

                 var checkin = checkinDate();

                 checkin.setHours(0,0,0,0);
                  //var checkinNew = new Date(checkin.getFullYear()+"-"+(checkin.getMonth())+"-"+checkin.getDay());


                 var checkout = checkoutDate();
                  var today = new Date();

                  today.setHours(0,0,0,0);

                  checkout.setHours(0,0,0,0);



                  //alert(today  + "     " + checkin  + "   ");


                  if(checkin < today){

                      $('#checkin_feedback').text("Check in Date must be today or greater");
                      $('#checkin_feedback').show();

                      errors = true;
                  }
                  else{
                     $('#checkin_feedback').hide();
                     
                  }

                  if(checkin >= checkout ){
                       $('#checkout_feedback').text("Checkout Date must be greater than checkin day");
                       $('#checkout_feedback').show();

                       errors = true;
                  }
                  else{
                     $('#checkout_feedback').hide();
                  }


                  if(errors == true){
                    return true;
                  }
                  else{
                    return false;
                  }


                }


                







        //hiding the warning when the user enters the room type selection
        $('#ReservationRoomType').focus(function () {
          $('#roomType_feedback').html(result).show().delay(2000).fadeOut();

        });

        //when the user leaves the box
        $('#ReservationRoomType').change(function () {

            //check the value
            checkRoom();
          });
        
        $('#searchBtn').click(function(event){

            var errors = validateDate();
            if(errors == true){
              event.preventDefault();
            }
            else{
              $('#RoomSearchSearchForm').submit();
            }


          });

      });
</script>


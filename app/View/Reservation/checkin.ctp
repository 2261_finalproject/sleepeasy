<h1 class="alert alert-info">Check in Guest</h1>
<?php

//debug($cost);

$checkinTime = strtotime($reservation['Reservation']['checkin']);
$checkoutTime = strtotime($reservation['Reservation']['checkout']);

$checkIn = date_create($reservation['Reservation']['checkin']);
$checkOut= date_create($reservation['Reservation']['checkout']);

 $diff = date_diff($checkIn, $checkOut);

$nights  = $diff->format("%d");
$cost = $nights * $reservation['RoomType']['cost'];



echo $this->Form->create('Reservation');
$options = array(
    '1' => 'Check in',
    '0' => 'Not Checked in'
);

echo $this->Form->input('Reservation.id',array('value'=>$reservation['Reservation']['id']));
echo $this->Form->input('Reservation.status', array(
    'type'      =>  'select',
    'options'   =>  $options
));


echo $this->Form->input('Reservation.numOfNights',array('value'=>$nights,'label'=>'Number of Nights'));
echo $this->Form->input('Charge.cost',array('value'=>$cost,'label'=>'Total Cost $'));


$options = array('label'=>'Check in Guest','class'=>'input-sm btn btn-success');



echo $this->Form->end($options);
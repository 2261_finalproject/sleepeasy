


<h1>Reservations</h1>





<table>
	<tr>
		<th><?php echo $this->Paginator->sort('User.last_name','Guest Name');?></th>
		<th><?php echo $this->Paginator->sort('RoomType.title','Room Type');?></th>
		<th><?php echo $this->Paginator->sort('Reservation.checkin','Checkin Date');?></th>
		<th><?php echo $this->Paginator->sort('Reservation.checkout','Checkout Date');?></th>
		<th><?php echo $this->Paginator->sort('Reservation.number_of_guests','Number of Guests');?></th>




	</tr>
	<!-- Here is where we loop through the rooms -->
	<?php foreach ($reservations as $reservation): ?>
		<tr>

			<td><?php echo $reservation['User']['first_name']. ' ' . $reservation['User']['last_name']; ?></td>
			<td><?php echo $reservation['RoomType']['title']; ?></td>
			<td><?php echo $reservation['Reservation']['checkin']; ?></td>
			<td><?php echo $reservation['Reservation']['checkout']; ?></td>
			<td><?php echo $reservation['Reservation']['number_of_guests']; ?></td>

			<td><?php echo $this->Html->link('Edit', 
                                        array('controller'=>'reservation', 'action'=>'edit', $reservation['Reservation']['id']),
                                        array('class'=>'btn btn-primary')); ?></td>
                        <td><?php echo $this->Html->link('Check in', 
                                        array('controller'=>'reservation', 'action'=>'checkin', $reservation['Reservation']['id']),
                                        array('class'=>'btn btn-success')); ?></td>
			<td><?php 
			echo $this->Form->postLink('Delete',
                                           array('controller'=>'reservation', 'action'=>'delete', $reservation['Reservation']['id']), 
                                            array('confirm'=>'Delete this reservation. Are you sure?', 'class'=>'btn btn-warning')); ?></td>





		</tr>
	<?php endforeach; ?>

</table>
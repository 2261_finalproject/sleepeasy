<?php
/**
 * Admin reservation list view
 * User: Tim
 * Date: 2/11/2015
 * Time: 8:14 PM
 * 
 * TODO: -display guest name properly
 *       -room number is a link to view that room?
 */
?>
<h1>Current Reservations</h1>
<table>
    <tr>
        <th>Checkin</th>
        <th>Checkout</th>
        <th>Room Number</th>
        <th>Number of Guests</th>
        <th>Guest Name</th>
    </tr>
    
    <?php foreach($reservations as $reservation) { ?>
    <tr>        
        <td><?php echo $reservation['Reservation']['checkin']; ?></td>
        <td><?php echo $reservation['Reservation']['checkout']; ?></td>
        <td><?php echo $reservation['Reservation']['room_number']; ?></td>
        <td><?php echo $reservation['Reservation']['number_of_guests']; ?></td>
    </tr>
    <?php } ?>
</table>
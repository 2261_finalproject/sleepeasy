<?php


 echo "<h2 class='alert alert-info'>".$reservation['User']['first_name']." ".$reservation['User']['last_name']."</h2>"; 

echo $this->Form->create('Reservation');
echo $this->Form->input('Reservation.id',array('display'=>'hidden',
	'value'=>$reservation['Reservation']['id']
	));




$checkinParts  = explode('-',$reservation['Reservation']['checkin']);
$checkoutParts  = explode('-',$reservation['Reservation']['checkout']);



echo $this->Form->input('Reservation.checkin', array(

	'selected'=>array(
		'day'=>$checkinParts[2],
		'month'=>$checkinParts[1],
		'year'=>$checkinParts[0]


		)
		,'minYear'=>date('Y')));

echo $this->Form->input('Reservation.checkout', array(

	'selected'=>array(
		'day'=>$checkoutParts[2],
		'month'=>$checkoutParts[1],
		'year'=>$checkoutParts[0]


		)
	,'minYear'=>date('Y')
	));


echo $this->Form->input('Reservation.number_of_guests',array(
		'value'=>$reservation['Reservation']['number_of_guests'],
		'min'=>'1',
		'max'=>'4'

	));

echo $this->Form->input('Reservation.discount' ,

 array(
	'value'=>$reservation['Reservation']['discount']
	,'min'=>'0'
	));




$options = array('label'=>'Update Reservation', 'class'=>'btn btn-primary');

echo $this->Form->end($options);
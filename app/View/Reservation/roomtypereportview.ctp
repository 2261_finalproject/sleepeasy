
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">

  
    
    

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {
             $.post('/sleepeasy/reservation/roomtypereport', function (result) {
                reservations = JSON.parse(result);
             // var obj = JSONObject(reservations[0][0])   ;
           //alert(reservations[1].room_types.title);
         // alert(accounts[1].Account.balance);
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Room Type');
        data.addColumn('number', 'Count');
        
        
        for(var i = 0; i < reservations.length; i++){
            data.addRow([
         reservations[i].room_types.title, parseInt(reservations[i][0].count)
          
        ]);
        }
        

      //   Set chart options
        var options = {'title':'Reservations By Type',
                       'width':600,
                       'height':500,
                       'pieSliceText':'value'
                       
        
                        };
                       

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
        });
      }
        
    </script>
    <div >
        <div  id="chart_div"></div>
    </div>

<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
        <?php echo $title ?>
	</title>
	<?php
		echo $this->Html->meta('favicon.ico','/img/favicon.ico',array('type'=>'icon'));

		echo $this->Html->css('cake.generic');
		echo $this->Html->css('bootstrap');
                echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
		echo $this->Html->css('styles');
                echo $this->Html->script('jquery');
               
                echo $this->Html->script('bootstrap.min');  
                
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    <script>
       $(document).ready(function() {
              $('.message').show().delay(4000).fadeOut();
        });
    </script>
</head>
<body>
    <div class="row height350">
        <h1>SleepEasy Hotel</h1>
    </div>
    <nav class="navbar navbar-inverse ">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsedBar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/sleepeasy/users/">Employee</a>
            </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="collapsedBar">
              <ul class="nav navbar-nav">
                <li><a href="/sleepeasy/"><i class="fa fa-home"> Home</i></a></li>
                
                
                <li><a href="/sleepeasy/charges/add"><i class="fa fa-credit-card"> Charges</i></a></li>
                
                <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-list"> Reservations </i><span class="caret"></span></a>
                  <ul class="dropdown-menu blueBack" role="menu">
                    <li><a href="/sleepeasy/reservation/viewreservations"><i class="fa fa-list-alt"> View Reservations</i></a></li>
                    <li><a href="/sleepeasy/reservation/checkoutlist"><i class="fa fa-sign-out"> Checkout Guest</i></a></li>
                  </ul>
                </li>
              </ul>

              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"> Account</i> <span class="caret"></span></a>
                  <ul class="dropdown-menu blueBack" role="menu">
                    <li><?php  echo "<a href='/sleepeasy/users/edit/".$loggedIn['id']."'><i class='fa fa-tasks'> Profile</i></a>";?></li>
                    <li class="divider"></li>
                    <li><a href="/sleepeasy/logout"><i class="fa fa-sign-out"> Logout</i></a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
   <?php echo $this->Session->flash(); ?>
	<div id="container" class="container">

		<div id="content">
                     
                      
			<?php echo $this->Session->flash('flashMessageSuccess'); ?>

			<?php echo $this->fetch('content'); ?>
		</div>

	</div>
    <?php echo $this->element('sql_dump');?>
</body>

</html>

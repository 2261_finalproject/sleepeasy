<?php
/**
 * Created by PhpStorm.
 * User: Kylea
 * Date: 2/10/2015
 * Time: 10:42 AM
 */
?>


<div class="content">
    <h1 class="alert alert-info">Amenities</h1>
    <ul>
        <li><h4>Free Wifi</h4></li>
        <li><h4>Digital Cable</h4></li>
        <li><h4>Dining Room/Pub</h4></li>
        <li><h4>24/7 Room Service</h4></li>
        <li><h4>Pet Friendly Rooms Available</h4></li>
    </ul>
    <?php echo $this->Html->image('george.jpg');?>
</div>

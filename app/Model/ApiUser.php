<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP ApiModel
 * @author jstillman
 */
class ApiUser extends AppModel {
    public $useTable = "users";
    
    
    
    
    public function beforeSave($options = array()) {
        if (!empty($this->data[$this->alias]['password'])) {
           
            $this->data[$this->alias]['password'] = sha1(
                    $this->data[$this->alias]['password']
            );

        }

        if(isset($this->data[$this->alias]['password_update'])&&!empty($this->data[$this->alias]['password_update'])){
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = sha1(
                    $this->data[$this->alias]['password_update']);
        }
        return true;
    }
    
    
}

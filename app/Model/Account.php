<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Account
 * @author jstillman
 */
class Account extends AppModel {
   public  $ownedBy= array(
      
      'User'=>array(
          'className'=> 'User',
          'foreignKey'=>'user_id'
      ));
}

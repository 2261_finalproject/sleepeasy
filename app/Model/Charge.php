<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Charge
 * @author jstillman
 */
class Charge extends AppModel {
 public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'ChargeType'=>array(
            'className'=>'ChargeType',
            'foreignKey' =>'charge_type'
        ),
        'Account'=>array(
            'className'=>'Account',
            'foreignKey'=>'account_id'
        )
     );
}

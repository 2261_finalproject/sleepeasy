<?php
/**
 * Created by PhpStorm.
 * User: Jordan
 * Date: 2015-01-15
 * Time: 8:44 AM
 */

include "AppModel.php";
class Post extends AppModel{

    public $validate = array(
        'title' => array(
            'rule' => 'notEmpty'
        ),
        'body' => array(
            'rule' => 'notEmpty'
        )
    );

}
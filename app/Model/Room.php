<?php
/**
 * Created by PhpStorm.
 * User: jstillman
 * Date: 1/17/2015
 * Time: 7:52 AM
 */

App::uses('AppModel', 'Model');

/**
 * @property Room $Room
 */
class Room  extends AppModel{
public $belongsTo = array(
        'RoomType' => array(
            'className' => 'RoomType',
            'foreignKey' => 'room_type'
        )
    );

public $primaryKey = "room_number";
}
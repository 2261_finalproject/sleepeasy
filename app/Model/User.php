<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    
    public $virtualFields = array('full_name' => 'CONCAT(first_name, " ", last_name)');

  
    
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required',
                'on'=>'create'

            )
        ),
        'first_name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'First name is required'
            )
        ),
        'last_name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Last name is required'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'email is required'
            )
        ),
         'address' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Address is required'
            )
        ),
         'city' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'City is required'
            )
        ),
         'postal_code' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Postal Code is required'
            )
        ),
         'phone' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Phone Number is required'
            )
        ),
         'payment_type' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Payment Type is required'
            )
        ),
         'cardholder_name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Card holder name is required'
            )
        ),
         'card_number' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Card Number is required'
            ),
             'credit_valid' => array(
                 'rule'=>array('cc','all',false,null),
                 'message'=>'Must be a valid credit card number'
             )
        ),
         'card_expiration' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Expiration date is required'
            )
        ),
    );

    public function beforeSave($options = array()) {
        if (!empty($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password']
            );

        }

        if(isset($this->data[$this->alias]['password_update'])&&!empty($this->data[$this->alias]['password_update'])){
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password_update']);
        }
        return true;
    }

}

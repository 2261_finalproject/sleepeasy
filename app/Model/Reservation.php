<?php
/**
 * Created by PhpStorm.
 * User: Jordan
 * Date: 2015-01-22
 * Time: 10:25 AM
 */

/**
 * @property Reservation $Reservation 
 */
class Reservation extends AppModel{

    public $belongsTo = array(
        'RoomType' => array(
            'className' => 'RoomType',
            'foreignKey' => 'room_type'
        ),

         'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Account'

    );
    
    //public $actsAs = array('Containable');

    
    public $validate = array(
        'room_type' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A Room Type is required',
                'allowEmpty'=>false
            )
        ),
        
    );

}
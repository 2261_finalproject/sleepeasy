<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP ReportsController
 * @author jstillman
 */
class ReportsController extends AppController {

    
    public function beforeFilter() {
        parent::beforeFilter();
         if($this->Auth->user('user_role')!='Manager'){
        $this->Session->setFlash('Not Authorized');
        $this->redirect('/');
    }
    }
    public function view() {
        
        $this->set('title','Reports || SleepEasy');
    }

}

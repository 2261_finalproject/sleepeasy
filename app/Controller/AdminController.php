<?php
/**
 * Created by PhpStorm.
 * User: jstillman
 * Date: 1/16/2015
 * Time: 7:35 AM
 */

App::uses('AppController', 'Controller');
class AdminController extends AppController {
   
    public function beforeFilter() {
        parent::beforeFilter();
        $this->layout="admin_layout";
        $this->Auth->allow(null);
        
    }
    public function index(){
        $this->set("title", 'Admin || SleepEasy');
    }
}
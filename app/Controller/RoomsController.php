<?php

/**
 * Created by PhpStorm.
 * User: jstillman & Tim FP
 * Date: 1/17/2015
 * Time: 7:39 AM
 */

App::uses('AppController', 'Controller');

/**
 * @property RoomsController $RoomsController
 */
class RoomsController extends AppController
{
    public $helpers = array('Html', 'Form');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('search');
        $this->Auth->deny('view','edit','delete','add');
    }

    public function index()
    {

    }

    /*
     * Displays the room list view, based on user type
     */
    public function view()
    {
        //if manager, show list
        if ($this->Auth->user('user_role') === "Manager") {
            $this->set('title', 'Room List || SleepEasy Admin');
            $this->set('rooms', $this->paginate('Room'));
        } else {
            // setting the message to the user 
            $this->Session->setFlash('Not Authorized to view this Page');
            //redirecting the user to the users home
            $this->redirect(array('controller' => 'users', 'action' => 'index'));
        }
        //$this->paginate('User');
    }
    
    /*
     * This function edits an existing room.
     */
    public function edit($room_number = null) {
        $this->set('title', 'Edit Room || SleepEasy Admin');

        $room = $this->Room->find('first',array('fields'=>array('room_number','room_type'),'conditions'=>array('room_number'=>$room_number))) ;

        //verify room exists
        if (!$room) {

            //setting the warning message
            $this->Session->setFlash('Unable to find that room');
            //redirecting the user to the home page
            $this->redirect('/');
        }

        //authorize only managers
        if ($this->Auth->user('user_role') === 'Manager') {

            //checking if this page has been submitted from the form
            if ($this->request->is('post') || $this->request->is('put')) {

                //checking if the room profile has been saved
                if ($this->Room->save($this->request->data)) {

                    //setting the success flash message
                    $this->Session->setFlash("Successfully updated Room", 'default', array('class' => 'message flashMessageSuccess'));

                    //redirect to room list page
                    return $this->redirect(array('action' => 'view'));
                }
                //if the user profile could not be saved
                $this->Session->setFlash(
                        __('The room could not be saved. Please, try again.')
                );
            } else {
                //if the page is accessed with a get request then set the request data 
                //equal to the user with the requested id
                $this->request->data = $this->Room->read(null, $room_number);
            }
        } else {
            $this->Session->setFlash('You do not have proper privileges to edit this room');
            $this->redirect('/');
        }
    }
    
    /*
     * This function deletes a room from the database.
     */
    public function delete($room_number = null){
        
        //only allow post requests
        $this->request->allowMethod('post');
        
        //authorize managers only
        if ($this->Auth->user('user_role') === "Manager") {

            $room = $this->Room->find('first',array('conditions'=>array('room_number'=>$room_number)));

            //check if room exists
            if (!$room) {
                throw new NotFoundException(__('Room Does not exist'));
            }

            $this->set('room', $room);

            //trying to delete the room
            if ($this->Room->delete($room['Room']['room_number'])) {

                //if successful set success message
                $this->Session->setFlash("Room Successfully Deleted", 'default', array('class' => 'message flashMessageSuccess'));

                //sending the user to the room list
                return $this->redirect(array('action' => 'view'));
            }

            //if the deletion was unsuccessful then warn the user and send to homepage
            $this->Session->setFlash(__('Room was not deleted'));
            return $this->redirect(array('action' => 'index'));
        } else {
            /* if the user is not authorized to view the page, warn them and send them to the 
              homepage */
            $this->Session->setFlash('Not Authorized to delete a room');
            $this->redirect(array('action' => 'index'));
        }
    }
    
    /***************************************************************************/
    /***************************************************************************/
    public function search(){
        $this->set('title','Search Rooms || SleepEasy');
        $data = $this->Room->RoomType->find('list',array('fields'=>array('id','title')));
        $this->set('roomTypes',$data);
        
        if($this->request->is('post')){
           $this->set('data',$this->request->data); 
        }
        
       
    }
    
    public function add(){
        
        $this->set('title','Add Room || SleepEasy');
        $data = $this->Room->RoomType->find('list',array('fields'=>array('id','title')));
            $this->set('roomTypes',$data);
        if($this->request->is('post')){
            $this->Room->create();
            
            if($this->Room->save($this->request->data)){
                $this->Session->setFlash("Successfully Added the room");
            }
            else{
                $this->Session->setFlash("Unable to Add room at this time");
            }
        }
        
        else{
            
        }
    }



}
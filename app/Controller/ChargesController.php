<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

/**
 * CakePHP ChargesController
 * @author jstillman
 */
class ChargesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->deny('weekReport','weekReportView','add');
    }
    
    
    public function userRole(){
        return $this->Auth->user('user_role');
    }
    
    
    
    public function weekReport() {
        $this->layout=null;
        $role = $this->userRole();
       if($role !='Manager'){
        $this->Session->setFlash('Not Authorized');
        $this->redirect('/');
    }
        
         $date = date('m');
         if($this->Auth->user('user_role')!= 'Manager'){
            $this->Session->setFlash('Must Be a manager');
            $this->redirect('/');
        }
         
         $charges = $this->Charge->find('all',array('fields'=>
             array('sum(Charge.charge_amount) AS total'),
             'conditions'=>array(
                 'MONTH(charge_datetime)'=>$date
                
             ),
             'group'=>array('charge_type')
         ));
         
         
         $this->set(
            'charges',json_encode($charges)
            
        );
         
         
         
    }
     public function weekReportView() {
         if($this->Auth->user('user_role')!= 'Manager'){
            $this->Session->setFlash('Must Be a manager');
            $this->redirect('/');
        }
         $this->set('title','Weekly Charge Report || SleepEasy');
         
         
     }
     
     public function add(){
         $role = $this->userRole();
       if($role != 'Manager' && $role != 'Employee'){
        $this->Session->setFlash('Not Authorized');
        $this->redirect('/');
    }
         
         $this->set('title','Charges || SleepEasy');
         
        
         
       $data = $this->Charge->User->find('list',array('fields'=>array( 'id', 'full_name')));
       $this->set('users',$data);
        $this->set('chargeType',$this->Charge->ChargeType->find('all',array('fields'=>'title')));
       
        
        
        if($this->request->is('post')){
           $charge = $this->request->data;
           $accountId=$this->Charge->query('SELECT id FROM accounts WHERE user_id='.$charge['Charge']['user_id']);
           $this->Charge->query("INSERT INTO charges (user_id,account_id,charge_amount,charge_type)VALUES (".$charge['Charge']['user_id'].",".$accountId[0]['accounts']['id'].",".
                   $charge['Charge']['charge_amount'].", '".$charge['Charge']['charge_type']."')");
           $this->set('charge',$accountId);
           
                          $this->Session->setFlash("Successfully Applied Charge", 'default', array('class' => 'message flashMessageSuccess'));

           $this->redirect('/');
        }
     }
}

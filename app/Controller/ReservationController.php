<?php

/**
 * Created by PhpStorm.
 * User: Jordan
 * Date: 2015-01-22
 * Time: 10:25 AM
 */
App::uses('AppController', 'Controller');

class ReservationController extends AppController {

    //public $uses = array('Charges', 'RoomType');
    public function beforeFilter() {
        parent::beforeFilter();


        //ensuring that the user must be logged in to search and book rooms

        $this->Auth->deny('search', 'booking', 'viewReservations','edit','checkin','checkOutList','checkout','roomTypeReport','roomTypeReportView');
    }

     public function userRole(){
        return $this->Auth->user('user_role');
    }
    
    
    
    /*
     * action to list reservations
     * gets all reservations with future checkout dates (ie ones that aren't in the past)
     */

    public function view() {
        //auth manager
        if ($this->Auth->user('user_role') === 'Manager') {
            $this->set('title', 'Reservations || SleepEasy Admin');

            date_default_timezone_set('America/Halifax');
            $reservations = $this->Reservation->find('all', array('Reservation.checkout >' => date('Y-m-d H:i:s')));
            $this->set('reservations', $reservations);
        }
    }

    public function search() {

        //setting the defualt time

        date_default_timezone_set('America/Halifax');

        //setting the title for the page

        $this->set('title', 'Reserve Room || SleepEasy');

        //passing the roomtypes to the view
        $this->set('roomType', $this->Reservation->RoomType->find('list', array(
                    'fields' => array('id', 'title')
        )));


        //checking if the room search form was submitted
        if ($this->request->is('post')) {

            //grabbing the submitted data
            $data = $this->request->data;
            $this->set('data', $data);


            // query just for testing purposes
            $query = " SELECT r.room_number FROM rooms r "
                    . "WHERE r.room_number NOT IN ( "
                    . "SELECT res.room_number FROM reservations res "
                    . "WHERE NOT (res.checkout < '2015-02-03' OR res.checkin > '2015-02-02') AND res.room_type='2') ORDER BY r.room_number";



            //query to get the available rooms

            $query2 = "SELECT r.room_number FROM rooms r 
WHERE r.room_type ='" . $data['Reservation']['room_type'] . "'
AND r.room_number NOT IN ( 
 SELECT res.room_number FROM reservations res 
 WHERE NOT (res.checkout > '" . $data['Reservation']['checkout']['year'] . '-' . $data['Reservation']['checkout']['month'] . "-" . $data['Reservation']['checkout']['day'] . "' 
     OR res.checkin < '" . $data['Reservation']['checkin']['year'] . '-' . $data['Reservation']['checkin']['month'] . "-" . $data['Reservation']['checkin']['day'] . "' )  


) ORDER BY r.room_number";


            //grabbing the results for both queries
            $result = $this->Reservation->query($query);
            $result2 = $this->Reservation->query($query2);


            // passing the form data to the session so that it will be available for booking
            $this->Session->write('room_type', $data['Reservation']['room_type']);
            $this->Session->write('checkin', $data['Reservation']['checkin']['year'] . '-' . $data['Reservation']['checkin']['month'] . "-" . $data['Reservation']['checkin']['day']);
            $this->Session->write('checkout', $data['Reservation']['checkout']['year'] . '-' . $data['Reservation']['checkout']['month'] . "-" . $data['Reservation']['checkout']['day']);
            $this->Session->write('num_of_guests', $data['Reservation']['number_of_guests']);

            //passing the data to the view
            $this->set('query', $result);
            $this->set('query2', $result2);
            $this->set('data', $data);
        }
    }

    /**
     * 
     * @param type $roomNumber
     * 
     * This method will attempt to book the room for the logged in user.
     * This method can only be hit from a post submission.
     */
    public function booking($roomNumber = null) {

        //Setting the allowed request method to post
        $this->request->allowMethod('post');

        //creating a blank reservation
        $this->Reservation->create();

        
        $account_id=$this->Reservation->query("SELECT id FROM accounts  WHERE user_id=".$this->Auth->user('id'));
        //setting the reservation details
        $this->Reservation->set('user_id', $this->Auth->user('id'));
        $this->Reservation->set('room_type', $this->Session->read('room_type'));
        $this->Reservation->set('checkin', $this->Session->read('checkin'));
        $this->Reservation->set('checkout', $this->Session->read('checkout'));
        $this->Reservation->set('number_of_guests', $this->Session->read('num_of_guests'));
        $this->Reservation->set('account_id',$account_id[0]['accounts']['id']);
        $this->Reservation->set('room_number', $roomNumber);

        //attempting to save the reservation to the database
        if ($this->Reservation->save()) {

            /* if the reservation was successfully made then display the success 
             * message and return the user to the home page.  
             */

            $this->Session->setFlash('Successfully Booked the Room', 'default', array('class' => 'message flashMessageSuccess'));
            $this->redirect('/');
        } else {

            /*
             * If there was a problem saving the reservation than warn the user
             * and send the user back to the room search form.
             */
            $this->Session->setFlash('Unable to Book the Room. Please Try Again.');
            $this->redirect('/roomsearch');
        }
    }

    public function viewReservations() {
        $this->set('title', 'View Reservations || SleepEasy');
        // $this->set('rooms', $this->paginate('Room'));
        
        
        if($this->userRole()!='Manager'&&$this->userRole()!='Employee'){
            $this->Session->setFlash('Not Authorized');
            $this->redirect('/');
        }
        
        
        date_default_timezone_set('America/Halifax');
        $date = date('Y-m-j');
        
        

        $this->set('reservations', $this->paginate('Reservation', array(
                    'checkin >=' => $date)));
    }
    
   public function delete($id = null) {

        //setting the method to be only accessed from post requests
        $this->request->allowMethod('post');
        //setting the proper authorization
        

            //setting the user id
            $this->Reservation->id = $id;

            //seeing if a user exist with the id 
            if (!$this->Reservation->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }

            //trying to delete the user
            if ($this->Reservation->delete()) {

                //if successful set success message
                $this->Session->setFlash("User Successfully Deleted", 'default', array('class' => 'message flashMessageSuccess'));

                //sending the user to the user list
                return $this->redirect('/');
            }

            //if the deletion was unsuccessful then warn the user and send to homepage
            $this->Session->setFlash(__('User was not deleted'));
            return $this->redirect(array('action' => 'index'));
        
    }


    public function edit($id = null) {


        //date_default_timezone_set('America/Halifax');


        $this->set('title', 'Edit Reservation || SleepEasy');
        
         if($this->userRole()!='Manager'&& $this->userRole()!='Employee'){
            $this->Session->setFlash('Not Authorized');
            $this->redirect('/');
        }

        if (!$id) {
            $this->redirect('/');
        }
        // $this->Reservation->id = $id;


        $reservation = $this->Reservation->find('first', array(
            'conditions' => array(
                'Reservation.id' => $id)));
        $this->set('reservation', $reservation);

        if ($reservation) {

            if ($this->request->is('get')) {
                $this->set('reservation', $reservation);
            } else if ($this->request->is('post') || $this->request->is('put')) {


                if ($this->Reservation->save($this->request->data)) {

                    $this->Session->setFlash('Successfully Booked the Room', 'default', array('class' => 'message flashMessageSuccess'));

                    $this->redirect('/');
                } else {
                    $this->Session->setFlash('Unable to update the reservation at this time');
                    $this->redirect('/');
                }
            }
        } else {
            $this->Session->setFlash('Unable to find that reservation');

            $this->redirect('/');
        }
    }

    public function checkin($id = null) {

         if($this->userRole()!='Manager'&&$this->userRole()!='Employee'){
            $this->Session->setFlash('Not Authorized');
            $this->redirect('/');
        }
        
        
        
        if (!$id) {
            $this->redirect('/');
        }

        $this->Reservation->id = $id;
        if ($this->Reservation->exists()) {
            $reservation = $this->Reservation->find('first', array(
                'conditions' => array(
                    'Reservation.id' => $id
                )
            ));

            $this->set('reservation', $reservation);
        }
        if ($this->request->is('post')) {
            //$this->set('info',$this->request->data);

            if ($this->Reservation->save($this->request->data)) {

                $account = $this->Reservation->query("SELECT id FROM accounts WHERE user_id=" . $reservation['User']['id'] . "");

                $accountId = $account[0]['accounts']['id'];

                $reservation2 = $this->request->data;



                $query = "INSERT INTO charges (account_id,user_id,charge_type,charge_amount) VALUES(" .
                        $accountId . ","
                        . $reservation['User']['id']
                        . ", 'Room Booking' ,"
                        . $reservation2['Charge']['cost'] . " )";
                $this->Reservation->query($query);
            }
        }
    }
    
    
    public function checkOutList(){
        
        
         if($this->userRole()!='Manager'&&$this->userRole()!='Employee'){
            $this->Session->setFlash('Not Authorized');
            $this->redirect('/');
        }
        
        
      $this->set('title','Checkout || SleepEasy');
      $reservations =  $this->Reservation->find('all',array(
            'conditions'=>array(
                'status'=>'1'
            )
           
           
        ));
      
      
      
      
      $this->set('reservations',$reservations);
    }
    
    
    public function checkout($id= null){
        $this->set('title','Checkout || SleepEasy');
        if(!$id){
            $this->Session->setFlash("No Reservation to checkout");
            $this->redirect('/');
        }
        $reservation = $this->Reservation->id = $id;
        
        
        if($this->Reservation->exists()){
            $reservationFound = $this->Reservation->find('first',array(
                'conditions'=>array('Reservation.id'=>$id)));
            $this->set('reservation',$reservationFound
            );
            
            if($this->request->is('post')){
                $formData = $this->request->data;
               $this->Reservation->query("INSERT INTO payments(user_id,account_id,payment_amount)VALUES (".$reservationFound['User']['id'].","
                   .$reservationFound['Account']['id'].",".$formData['Checkout']['pay'].")");
               
               $this->Reservation->query("UPDATE reservations SET status='0' WHERE id=".$reservationFound['Reservation']['id']);
              
                $this->Session->setFlash("Successfully Checked out user", 'default', array('class' => 'message flashMessageSuccess'));
               $this->redirect('/');
               
               
            }
            
        }else{
            $this->Session->setFlash('Unable to find that reservation');
            $this->redirect('/');
        }
       
    }
    
    
    public function roomTypeReport(){
        $this->layout =null;
        $date = date('m');
        $reservations = $this->Reservation->query("SELECT reservations.room_type, COUNT(*)AS count,room_types.title  FROM reservations JOIN room_types ON reservations.room_type = room_types.id GROUP BY room_type");
         $this->set(
            'reservations',json_encode($reservations)
            
        );
    }
     public function roomTypeReportView(){
         $this->set('title','Room Type Report || SleepEasy');
    
    
     }

}

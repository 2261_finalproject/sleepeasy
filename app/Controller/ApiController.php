<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');


/**
 * CakePHP RestUsersController
 * @author jstillman
 * 
 * This controller is used a restful api
 */

//Controller::loadModel('User');
class ApiController extends AppController {

    public $uses = array('ApiUser', 'RoomType');
    public $helpers = array('Html', 'Form');
    public $components = array('RequestHandler',
      'Auth' => array(
            
            'authenticate' => array(
                'Basic' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )));

    
    public function beforeFilter() {
        parent::beforeFilter();
         $this->RequestHandler->ext = 'json';
         $this->layout = null;
       // $this->Auth->deny('getRoomTypes');


         //$this->Auth->authenticate = array('Digest');
        
    }
    
    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    
    /**
     * This method will return an array of either json or xml based on the extension
     * that the call is made with
     */
    public function getUserDetails() {
        
        /*
         * Grabbing all of the users and storing them in an array. Defining the
         * fields the response will hold
         */
        
        if($this->Auth->user()){
        $users = $this->User->find('all', array('fields' =>
            array('id', 'username', 'email', 'first_name', 'last_name')));
        $success = array('status'=>'1');
        $response = array_merge($users,$success);
        //passing the values, and serializing the response
        $this->set(array(
            'users' => $response,
            '_serialize' => array('users')
        ));}
        else{
                    $success = array('status'=>'0');
$this->set(array(
            'users' => $success,
            '_serialize' => array('users')
        ));
        }
        
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * This method will return all of the room types.
     */
    public function getRoomTypes() {
        
        //getting all of the room types and serailizing the results
        
        if($this->Auth->user()){
        $roomTypes = $this->RoomType->find('all', array('fields' =>
            array('id', 'title', 'description')));
        $success = array('status'=>'1');
        $response = array_merge($roomTypes,$success);
        
        $this->set(array(
            'roomTypes' => $response,
            '_serialize' => array('roomTypes')
        ));
        }else{
              $success = array('status'=>'0');
       $this->set(array(
            'roomTypes' => $success,
            '_serialize' => array('roomTypes')
        ));
        }
    }
    
    /**
     * When hitting this from the outside use User[username] and User[password] 
     * as the headers
     */
    public function login(){
        
       // $this->autoRender = faslse;
        $this->request->allowMethod('post');
        $this->layout=null;

$info= $this->request->input('json_decode');

//$this->set('data',$data);

 $this->set(array(
             'users' => $info,
             '_serialize' => array('users')
             ));



        // if($this->request->data)

        // 	//$this->request->data = json_decode($this->request->data);
        // $this->Auth->logout();



        //   if( $this->Auth->login()){
              
        //       $loggedInUser = $this->Auth->user();
        //       $success = array('status'=>'1');
        //       $response = array_merge($loggedInUser,$success);
        //          $this->set(array(
        //     'user' => $response,
        //     '_serialize' => array('user')
        //     ));//}
        //   }
          
          
        //     else{
        //         $success = array('status'=>'0');
        //         $this->set(array(
        //     'user' => $success,
        //     '_serialize' => array('user')
        //     ));
        //     }
   

}

public function getUser($request) {
    $username = env('PHP_AUTH_USER');
    $pass = env('PHP_AUTH_PW');

    if (empty($username) || empty($pass)) {
        $this->set(array(
            'user' => $success,
            '_serialize' => $this->_findUser($username, $pass)
            ));
    }
    

    $this->set(array(
            'user' => $success,
            '_serialize' => $this->_findUser($username, $pass)
            ));
    return ;
}



       

 public function register(){


//$passwordHasher = new SimplePasswordHasher();

$this->layout = null;

$this->request->allowMethod('post'); 


if(isset($this->request->data)){
  $this->ApiUser->create();

 // $this->request->data('password') = sha1($this->request->data('password'));
//$passwordHash = $passwordHasher->hash()
try{
  $this->ApiUser->save($this->request->data);
 // $this->User->set('password',$passwordHash);


  $this->set(array(
            'user' => 'Success',
            '_serialize' => array('user')
            ));

}

catch(Exception $e){
$this->set(array(
            'user' => 'Failure',
            '_serialize' => array('user')
            ));

}

}}}
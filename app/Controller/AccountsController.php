<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

class AccountsController extends AppController {

public $components = array('RequestHandler');

public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->deny('report','reportview');
    
    if($this->Auth->user('user_role')!='Manager'){
        $this->Session->setFlash('Not Authorized');
        $this->redirect('/');
    }
}





public function report(){
    
    $this->RequestHandler->ext = 'json';
    $this->layout = null;
    $accounts = $this->Account->find('all',
            array(
                'conditions'=>array(
                    'balance >'=>0
                ),
            )
            );
    
   
   $this->set('report',  json_encode($accounts
        ));
    
    
  
   
    
}

public function reportView(){
    $this->set('title','Account Balance Report || SleepEasy');
}




    
}

<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
//App:users('Auth','Auth');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $components = array(
        'Session',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'users',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'index',
                'home'
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            )
        )
    );

    public function beforeFilter()
            
            
    {
        //set layout based on auth level - Tim FP
        switch($this->Auth->user('user_role')){
            case 'Guest':
                $this->layout = 'default';
                break;
            case 'Employee':
                $this->layout = 'emp_layout';
                break;
            case 'Manager':
                $this->layout = 'admin_layout';
        }
        
        $this->set('loggedIn',$this->Auth->user());
        
        if(in_array($this->params['controller'],array('rest_users'))){
        // For RESTful web service requests, we check the name of our contoller
        $this->Auth->allow('index');
        $this->Security->allowedControllers('user');
        // this line should always be there to ensure that all rest calls are secure
        // $this->Security->requireSecure(); 
        // $this->Security->unlockedActions = array('index');
        // SecurityComponent::allowedControllers('users');
         
        }else{
            // setup out Auth
            $this->Auth->allow();         
        }
//        $this->Auth->allow('index');
    }
    public function isAuthorized($user) {
        // Admin can access every action
//        if (isset($user['role']) && $user['role'] === 'manager') {
//            return true;
//        }
//
//        // Default deny
//        return false;
    }

}

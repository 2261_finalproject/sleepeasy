<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {
    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    //adding some of the helper classes;
    public $helpers = array('Html', 'Form');

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * function calls the parent beforeFilte to set authorized pages,
     * then sets custom pages that the user is allowed to view.
     */
    public function beforeFilter() {

        parent::beforeFilter();
        $this->Auth->deny('edit', 'delete', 'view');
        $this->Auth->allow('index', 'register', 'login', 'logout', 'check_username');
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * This will be the main landing page for the users
     */
    public function index() {

        //setting the title for the page
        $this->set('title', 'Users Home || SleepEasy');
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * This method will give the abilitity to view all of the users if the logged
     * in user has role of manager, otherwise the user will be redirected to the
     * user home page
     * 
     */
    public function view() {

        //getting the user role for the logged in the user
        $result = UsersController::isAuthorized($this->Auth->user('user_role'));

        //checking if a manager
        if ($result == 2) {

            //setting a paginate and setting the title
            $this->User->recursive = 0;
            $this->set('users', $this->paginate('User'));
            $this->set('title', 'User List || SleepEasy Admin');
        } else {
            // setting the message to the user 
            $this->Session->setFlash('Not Authorized to view this Page');
            //redirecting the user to the users home
            $this->redirect(array('controller' => 'users', 'action' => 'index'));
        }
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * 
     * This method will be used to register the user into the database
     * 
     */
    public function register() {

        //setting the title
        $this->set('title', 'Create Account || SleepEasy');

        //checking if the form is submitted
        if ($this->request->is('post')) {

            //creating the user
            $this->User->create();

            //trying to set the user into the database
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash("Successfully Registered", 'default', array('class' => 'message flashMessageSuccess'));
                $this->Auth->login();
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                    __('The user could not be saved. Please, try again.')
            );
        }
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * 
     * @param type $id
     * @return type
     * @throws NotFoundException
     * 
     * This method will be used to modify the user profile.
     * It will display different messages based on success or failure of the 
     * modifications.
     */
    public function edit($id = null) {

        //setting the title for the page;
        $this->set('title', 'Edit User || SleepEasy Admin');
        $this->set('roles',array('Manager','Employee','Guest'));
        //setting the user id
        $this->User->id = $id;

        //seeing if a user with the specified id exists
        if (!$this->User->exists()) {

            //setting the warning message
            $this->Session->setFlash('Unable to find that user');
            //redirecting the user to the home page
            $this->redirect('/');
        }

        //Checking to see if the user being edited is the logged in user or a manager
        if ($id == $this->Auth->user('id') || $this->Auth->user('user_role') === 'Manager') {

            //checking if this page has been submitted from the form
            if ($this->request->is('post') || $this->request->is('put')) {

                //checking if the user profile has been saved
                if ($this->User->save($this->request->data)) {

                    //setting the success flash message
                    $this->Session->setFlash("Successfully updated Profile", 'default', array('class' => 'message flashMessageSuccess'));

                    //sending the user to the user list
                    return $this->redirect(array('action' => 'view'));
                }
                //if the user profile could not be saved
                $this->Session->setFlash(
                        __('The user could not be saved. Please, try again.')
                );
            } else {
                //if the page is accessed with a get request then set the request data 
                //equal to the user with the requested id
                $this->request->data = $this->User->read(null, $id);

                //unsetting the password so that the user must re enter their password
                unset($this->request->data['User']['password']);
            }
        } else {
            $this->Session->setFlash('You do not have proper privileges to edit this user');
            $this->redirect('/');
        }

        //checking if a user with that id exists
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * 
     * @param type $id
     * @return type
     * @throws NotFoundException
     * 
     * This method will be used to delete user accounts , it will look for proper
     * authization to do this.
     */
    public function delete($id = null) {

        //setting the method to be only accessed from post requests
        $this->request->allowMethod('post');
        //setting the proper authorization
        if ($this->isAuthorized($this->Auth->user('user_role')) == 2) {

            //setting the user id
            $this->User->id = $id;

            //seeing if a user exist with the id 
            if (!$this->User->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }

            //trying to delete the user
            if ($this->User->delete()) {

                //if successful set success message
                $this->Session->setFlash("User Successfully Deleted", 'default', array('class' => 'message flashMessageSuccess'));

                //sending the user to the user list
                return $this->redirect(array('action' => 'view'));
            }

            //if the deletion was unsuccessful then warn the user and send to homepage
            $this->Session->setFlash(__('User was not deleted'));
            return $this->redirect(array('action' => 'index'));
        } else {

            /* if the user is not authorized to view the page, warn them and send them to the 
              homepage */
            $this->Session->setFlash('Not Authorized to delete a user');
            $this->redirect(array('action' => 'index'));
        }
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * 
     * @return redirect
     * 
     * This method will be used to login the user.
     * 
     */
    public function login() {
        $this->Auth->logout();
        //setting the title for the page
        $this->set('title', 'User Login || SleepEasy');

        //checking if the login form was submitted
        if ($this->request->is('post')) {
            //checking if the user was successfully logged in
            if ($this->Auth->login()) {

                //setting the flash message for successful login
                $this->Session->setFlash("Successfully logged in", 'default', array('class' => 'message flashMessageSuccess'));

                //sending the user to their original destination
                return $this->redirect($this->Auth->redirectUrl());
            }
            /* if there is an incorrect username or password than warn the user 
              and keep them on the same page.
             */
            $this->Session->setFlash(__('Invalid username or password, try again'));
        }
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * This method will be used for loggin out the user
     */
    public function logout() {

        //calling the authentication component logout function
        $this->Auth->logout();

        //setting the successful logout message
        $this->Session->setFlash('Successfully Logged out', 'default', array('class' => 'message flashMessageSuccess'));
        //sending the user to the home page
        $this->redirect(array('action' => 'index'));
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * 
     * @param type $user
     * @return int
     * 
     * Overiding the parent isAuthorized method.
     * This method will be used by the other methods to check if a user is 
     * authorized to perform diffrent actions
     */
    public function isAuthorized($user) {
        //$user = $this->Auth->user('user_role');
        if ($user === "Manager") {
            return 2;
        } else if ($user === "Employee") {
            return 1;
        } else {
            return 0;
        }
    }

    /* ----------------------------------------------------------------------
     * ---------------------------------------------------------------------- */

    /**
     * This method will be used for the for the registraion form to see if the 
     * username the user wants to use is already in use.
     */
    function check_username() {
        //checking if page is being directly accessed and displaying warning
        if ($this->request->is('get')) {
            $this->Session->setFlash('Can\'t directly access the username check');
            $this->redirect(array('controller' => 'users', 'action' => 'index'));
        }

        //turning the layout off
        $this->autoRender = false;


        //getting the username that has been passed through
        $username = $_POST['username'];


        //running the query to get the number of users
        $query = $this->User->find('count', array(
            'conditions' => array('User.username' => $username)
        ));

        //checking if the username was blank
        if (strlen($username) == 0) {

            echo "Choose a username";
        }
        //if the username is too short
        else if (strlen($username) < 5) {
            echo "Username is too short!";
        }
        //if the query executed returns rows then the username is taken
        else if ($query != 0) {

            echo "Username Taken!";
        }
        //let the user register
        else {

            echo "Username available";
        }
    }

}
